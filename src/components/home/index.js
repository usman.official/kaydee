import React from 'react'

import './style.scss'
import SearchBar from 'components/search-input'

const Home = () => (
  <div className="app__container">
    <h1 style={{ margin: '20px 0' }} className="text--center">
      Stay up to date with your favourite Artist
    </h1>
    <img
      className="text--center img--medium"
      src="/static/CYLO_BAND.gif"
      alt="rockit"
    />
    <SearchBar showSuggestions={true} />
  </div>
)

export default Home
