/* eslint-disable camelcase */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import LinearProgress from '@material-ui/core/LinearProgress'
import IconButton from '@material-ui/core/IconButton'

import './style.scss'

const ProfileEvents = props => {
  const { artistEvents = [] } = props
  const [selectedEvent, setSelectedEvent] = useState(null)
  const [isLoadingIframe, setIsLoadingIframe] = useState(false)
  return (
    <div className="events">
      <h2>
        <b>{`"${artistEvents.length}"`}</b>
        {` EVENTS IN YOUR AREA`}
      </h2>
      <div className="events__container">
        <div className="events__container--list">
          {artistEvents.map(event => {
            const { venue, description = '', url } = event
            return (
              <Paper
                key={event.id}
                className={`event__item${
                  (selectedEvent || { id: '-1' }).id === event.id
                    ? ' event__item--selected'
                    : ''
                }`}
                onClick={() => {
                  setSelectedEvent(event)
                  setIsLoadingIframe(true)
                }}
              >
                <p className="event__item--name">
                  <b>{`${venue.name}`}</b>
                </p>
                <p>
                  City: <b>{`${venue.city}`}</b>
                </p>
                <p>
                  Region: <b>{`${venue.region}`}</b>
                </p>
                <p>
                  Country: <b>{`${venue.country}`}</b>
                </p>
                <p className="event__item--description">
                  <i>{description}</i>
                </p>
                {url.startsWith('https://www.bandsintown.com') && (
                  <div className="event__item--social-links">
                    <IconButton
                      onClick={() => {
                        window.open(url, '_blank')
                      }}
                    >
                      <img
                        src="https://assets.bandsintown.com/images/bitFist.svg"
                        alt="Bandsintown Fist"
                      />
                    </IconButton>
                  </div>
                )}
              </Paper>
            )
          })}
        </div>
        {artistEvents.length > 0 && (
          <div className="events__container--embed">
            {isLoadingIframe && <LinearProgress />}
            {!selectedEvent && (
              <div className="info__message">
                <p>Select an Event to view details</p>
              </div>
            )}
            {selectedEvent && (
              <iframe
                title="brandistown"
                src={selectedEvent.url}
                onLoad={() => {
                  setIsLoadingIframe(false)
                }}
              />
            )}
          </div>
        )}
      </div>
    </div>
  )
}

export default ProfileEvents

ProfileEvents.propTypes = {
  artistEvents: PropTypes.arrayOf(PropTypes.object),
}
