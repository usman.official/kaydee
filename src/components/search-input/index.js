import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { navigate } from '@reach/router'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import TextField from '@material-ui/core/TextField'
import SearchIcon from '@material-ui/icons/Search'

import SearchSuggestions from 'components/search-suggestions'

import { en } from 'lang/en'
import { SEARCH_ARTIST_URL } from 'libs/apis'

import './style.scss'

const SearchBar = props => {
  const { showSuggestions = false } = props
  const [searchText, setSearchText] = useState('')
  const [searchResults, setSearchResults] = useState([])
  const [isFetching, setIsFetching] = useState(false)
  useEffect(() => {}, [searchResults])
  return (
    <div className="search__text">
      <TextField
        id="artist-name-search"
        variant="filled"
        margin="dense"
        hiddenLabel
        fullWidth
        value={searchText}
        placeholder={en.input_search_artists_placeholder}
        InputProps={{
          inputProps: {
            'aria-label': 'search artist',
          },
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="search"
                size="small"
                onClick={() => {
                  handleSearchClick({ searchText })
                }}
              >
                <SearchIcon fontSize="inherit" />
              </IconButton>
            </InputAdornment>
          ),
        }}
        onChange={e => {
          setSearchText(e.target.value)
          const artistName = e.target.value || ''
          if (artistName !== '') {
            setIsFetching(true)
            axios
              .get(`${SEARCH_ARTIST_URL}/${artistName}`, {
                params: {
                  app_id: '123',
                },
              })
              .then(response => {
                if (response.data) {
                  let isExist = false
                  searchResults.forEach(result => {
                    if (result.id === response.data.id) {
                      isExist = true
                    }
                  })
                  if (!isExist) {
                    setSearchResults([...searchResults, response.data])
                  }
                }
              })
              .catch(error => {
                console.log({ error })
              })
              .finally(() => {
                setIsFetching(false)
              })
          }
        }}
      />
      {showSuggestions && (
        <SearchSuggestions
          isFetching={isFetching}
          searchResults={
            searchText === ''
              ? []
              : searchResults.filter(searchResult =>
                (searchResult.name || '').startsWith(searchText),
              )
          }
          searchText={searchText}
        />
      )}
    </div>
  )
}

export default SearchBar

SearchBar.propTypes = {
  showSuggestions: PropTypes.bool,
}

const handleSearchClick = ({ searchText }) => {
  navigate(`/artists/${searchText}`)
}
