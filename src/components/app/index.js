import React from 'react'
import { Router } from '@reach/router'

import AppHeader from 'components/app-header'
import Home from 'components/home'
import Profile from 'components/profile'

const App = () => (
  <div className="app">
    <AppHeader />
    <Router>
      <Home path="/" />
      <Profile path="/artist/:artistName" />
      <Home path="/artists/:artistName" />
    </Router>
  </div>
)

export default App
