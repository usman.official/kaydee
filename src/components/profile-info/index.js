/* eslint-disable camelcase */
import React from 'react'
import PropTypes from 'prop-types'
import IconButton from '@material-ui/core/IconButton'
import FacebookIcon from '@material-ui/icons/Facebook'

import './style.scss'

const ProfileInfo = props => {
  const {
    id,
    name,
    thumb_url,
    image_url,
    upcoming_event_count,
    url,
    facebook_page_url,
  } = props.artist
  return (
    <div id={id} className="info__container">
      <img
        className="info__container--picture"
        src={`${thumb_url || image_url}`}
        alt="profile-dp"
      />
      <div className="control__group">
        <p className="profile__name">{`${name}`}</p>
      </div>
      <div className="control__group">
        <div className="social__links">
          <p>
            {`Upcomming Events: `}
            <b>{`${upcoming_event_count}`}</b>
          </p>
          <div className="social__links--btns">
            {facebook_page_url && (
              <IconButton
                onClick={() => {
                  window.open(facebook_page_url, '_blank')
                }}
              >
                <FacebookIcon fontSize={'large'} />
              </IconButton>
            )}
            {url && (
              <IconButton
                onClick={() => {
                  window.open(url, '_blank')
                }}
              >
                <img
                  src="https://assets.bandsintown.com/images/bitFist.svg"
                  alt="Bandsintown Fist"
                />
              </IconButton>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProfileInfo

ProfileInfo.propTypes = {
  artist: PropTypes.object,
}
