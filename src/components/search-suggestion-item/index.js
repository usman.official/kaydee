/* eslint-disable camelcase */
import React from 'react'
import PropTypes from 'prop-types'
import Avatar from '@material-ui/core/Avatar'

import { KEY_DOWN_EVENT } from 'libs/apis'

import './style.scss'

const SearchSuggestionListItem = props => {
  const {
    id = '-1',
    mbid = '-1',
    name = 'Unnamed Artist',
    facebook_page_url = '',
    tracker_count = 0,
    image_url = 'https://cdn.iconscout.com/icon/free/png-256/avatar-372-456324.png',
    thumb_url = 'https://cdn.iconscout.com/icon/free/png-256/avatar-372-456324.png',
    upcoming_event_count = 0,
    url = '',
    onClick,
  } = props
  return (
    <div
      id={id}
      className="search__suggetstions--item"
      role="menuitem"
      tabIndex={id}
      onClick={() => {
        onClick &&
          onClick({
            id,
            mbid,
            name,
            facebook_page_url,
            tracker_count,
            image_url,
            thumb_url,
            upcoming_event_count,
            url,
          })
      }}
      onKeyDown={KEY_DOWN_EVENT}
    >
      <div className="item__avatar">
        <Avatar src={thumb_url || image_url} />
      </div>
      <div className="item__details">
        <div className="item__details--primary">{name}</div>
        <div className="item__details--secondary">
          {`Upcomming Events:`}
          <b>{upcoming_event_count}</b>
        </div>
      </div>
    </div>
  )
}

SearchSuggestionListItem.propTypes = {
  id: PropTypes.string,
  mbid: PropTypes.string,
  name: PropTypes.string,
  facebook_page_url: PropTypes.string,
  image_url: PropTypes.string,
  thumb_url: PropTypes.string,
  url: PropTypes.string,
  upcoming_event_count: PropTypes.number,
  tracker_count: PropTypes.number,
  onClick: PropTypes.func,
}

export default SearchSuggestionListItem
