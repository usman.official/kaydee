import React from 'react'
import PropTypes from 'prop-types'
import { navigate } from '@reach/router'

import SearchSuggestionListItem from 'components/search-suggestion-item'

import { en } from 'lang/en'

import './style.scss'

const SearchSuggestions = props => {
  const { searchResults = [], isFetching = false, searchText = '' } = props
  if (isFetching) {
    return (
      <div className="search__suggestions--searching">
        <img src="/static/monkey.gif" alt="searching" />
        <p>{en.searching_artists}</p>
      </div>
    )
  }
  if (!isFetching && searchResults.length > 0) {
    return (
      <div className="search__suggestions">
        {searchResults.map(result => {
          if (result.id) {
            return (
              <SearchSuggestionListItem
                key={result.id}
                {...result}
                onClick={handleArtistClick}
              />
            )
          }
        })}
      </div>
    )
  }
  if (!isFetching && searchText !== '' && searchResults.length === 0) {
    return (
      <div className="search__suggestions--searching">
        <p>{en.searching_no_artists_found({ artistName: searchText })}</p>
      </div>
    )
  }
  return null
}
SearchSuggestions.propTypes = {
  searchResults: PropTypes.arrayOf(PropTypes.object),
  isFetching: PropTypes.bool,
  searchText: PropTypes.string,
}
export default SearchSuggestions

const handleArtistClick = artist => {
  navigate(`/artist/${artist.name}`, { state: { artist } })
}
