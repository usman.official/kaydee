import React from 'react'
import { navigate } from '@reach/router'

import './style.scss'

const AppHeader = props => (
  <div className="app__header">
    <div
      role="button"
      tabIndex={0}
      onKeyDown={handleKeyDown}
      className="logo"
      onClick={() => {
        navigate('/')
      }}
    >
      <img width="50px" src="/static/rockit.gif" alt="rockit" />
      KayDee
    </div>
  </div>
)

export default AppHeader

const handleKeyDown = e => {
  console.log(e)
}
