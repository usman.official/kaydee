import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { navigate } from '@reach/router'
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

import ProfileInfo from 'components/profile-info'
import ProfileEvents from 'components/profile-events'

import { getArtistInfo, KEY_DOWN_EVENT } from 'libs/apis'

import './style.scss'

const Profile = props => {
  const { artistName = '' } = props
  const [isFetchingArtist, setIsFetchingArtist] = useState(false)
  const [isFetchingEvents, setIsFetchingEvents] = useState(false)
  const [artistInfo, setArtistInfo] = useState({ id: '-1', artistName })
  const [artistEvents, setArtistEvents] = useState([])
  useEffect(() => {
    getArtistInfo({
      artistName,
      setArtistInfo,
      setArtistEvents,
      setIsFetchingArtist,
      setIsFetchingEvents,
    })
  }, [artistName])
  if (artistName === '') {
    return (
      <div className="profile">
        <h1>{`No profile found for "${artistName}"`}</h1>
      </div>
    )
  }
  console.log({
    artistInfo,
    artistEvents,
  })
  return (
    <div className="profile">
      <div className="profile__header">
        <IconButton
          onClick={() => {
            navigate('/')
          }}
          onKeyDown={KEY_DOWN_EVENT}
        >
          <ArrowBackIcon />
        </IconButton>
        <h2>{`Profile/${artistInfo.name || artistName || ''}`}</h2>
      </div>
      <div className="profile__container">
        {isFetchingArtist && <div>Loading Artist Information</div>}
        {!isFetchingArtist && artistInfo && (
          <Paper className="profile__container--info">
            <ProfileInfo artist={artistInfo} />
          </Paper>
        )}
        <div className="profile__container--events">
          {isFetchingEvents && <div>Loading Events Information</div>}
          {!isFetchingArtist && !isFetchingEvents && (
            <ProfileEvents artistEvents={artistEvents} />
          )}
        </div>
      </div>
    </div>
  )
}

export default Profile

Profile.propTypes = {
  artistName: PropTypes.string,
}
