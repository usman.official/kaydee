export const en = {
  cookie_message: `We use cookies to give you the best online experience. By using our website you agree to our use of cookies in accordance with our cookie policy.`,
  input_search_suggestions_placeholder: `Search for artists and concerts`,
  input_search_artists_placeholder: `Enter the name of the artist you want to search`,
  searching_artists: `Searching the web for artists ...`,
  searching_no_artists_found: ({ artistName = '' }) =>
    `${
      artistName !== '' ? `"${artistName}"` : 'Artist'
    } not found on this planet !`,
}
