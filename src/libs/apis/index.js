import axios from 'axios'

export const SEARCH_ARTIST_URL = `https://rest.bandsintown.com/artists`

export const KEY_DOWN_EVENT = e => {
  console.log(e)
}

export const getArtistInfo = ({
  setIsFetchingArtist,
  artistName,
  setArtistInfo,
  setArtistEvents,
  setIsFetchingEvents,
}) => {
  setIsFetchingArtist(true)
  axios
    .get(`${SEARCH_ARTIST_URL}/${artistName}`, {
      params: {
        app_id: '123',
      },
    })
    .then(response => {
      if (response.data) {
        if (response.data.id) {
          setArtistInfo(response.data)
          getArtistEvents({
            artistName,
            setArtistEvents,
            setIsFetchingEvents,
          })
        }
      }
    })
    .catch(error => {
      console.log({ error })
    })
    .finally(() => {
      setIsFetchingArtist(false)
    })
}

const getArtistEvents = ({
  artistName,
  setArtistEvents,
  setIsFetchingEvents,
}) => {
  setIsFetchingEvents(true)
  axios
    .get(`${SEARCH_ARTIST_URL}/${artistName}/events`, {
      params: {
        app_id: '123',
      },
    })
    .then(response => {
      if (response.data) {
        if (response.data.length > 0) {
          setArtistEvents(response.data)
        }
      }
    })
    .catch(error => {
      console.log({ error })
    })
    .finally(() => {
      setIsFetchingEvents(false)
    })
}
