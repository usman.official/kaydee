## Demo
[Click here to Visit Site](https://kaydee-demo.000webhostapp.com/)

## Description

This is a challenge take home assignment.

## CODING STANDARDS

To maintain code quality and standards following point were considered:

The Application architecture is based on latest REAECT HOOKS techniques and REDUX framework.

The application uses MATERIAL UI Components to maintain the consistency in design. 

For Styling the B.E.M standards are used to maintain the CSS/SCSS Standards.

To Keep the code clean ESLINT is applied.

The Name of components/variables is kept self explainatory.i.e., the Code itself is the documentation.

## NPM Scripts

You can run following comands:

- `yarn start`: For daily local development
- `yarn debug`: Similar as `yarn start`, a little slower then `yarn start` but has more accurate sourcemap and compatible styles
- `yarn build`: For production build
- `yarn analyze`: Run build analyzer
- `yarn format`: Format all your code
- `yarn lint`: Run code style checker

## Configure local proxy

You can edit `/dev-proxy.js` to apply proxy for your local devlopment enviroment.

## Upgrade dependencies

Use `yarn upgrade-interactive` to upgrade dependencies. _**Nerver**_ edit `package.json` manually.

## Run in Docker for devlopment

Run `docker-compose -f docker-compose-local.yml up`.

## FUTURE WORK

Due to limited time given for challenge following things are missing:

-> UNIT Tests (Enzyme & JEST)

-> TYPESCRIPT

The above mentioned items are required to keep the Code base upto date with the latest standards.